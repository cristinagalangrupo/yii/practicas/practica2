<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;

class SiteController extends Controller
{
    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    public function actionNoticias()
    {
        $datos= \app\models\Noticias::find()->all();
        return $this->render("noticias",[
                "noticias"=>$datos]);
    }
    public function actionArticulos(){
        $datos= \app\models\Articulos::find()->all();
        return $this->render('articulos',[
            "articulos"=>$datos
        ]);
    }
    public function actionArticuloampliado($id){
        $articulos= \app\models\Articulos::find()
                ->where(['id'=>$id])
                ->one();
        //$dataProvider=new ActiveDataProvider
        
        return $this->render("articuloAmpliado",[
            "id"=>$id,
            "articulos"=>$articulos,
        ]);
    }
    public function actionTodo(){
        
        
        $datos= \app\models\Noticias::find();
        $datos1= \app\models\Articulos::find();
        
        $dataProvider = new ActiveDataProvider([
            'query' => $datos,
            'pagination'=>[
                'pageSize'=>1,
                'pageParam'=>'uno',
            ]
        ]);
        $dataProvider1 = new ActiveDataProvider([
            'query' => $datos1,
            'pagination'=>[
                'pageSize'=>1,
                'pageParam'=>'dos',
            ]
        ]);
        

       
        return $this->render('todo',[
            "dataProvider"=>$dataProvider,
            "dataProvider1"=>$dataProvider1,
        ]);
    }

    
}
