<?php
    use yii\grid\GridView;
    use yii\helpers\Html;
    
?>

<?= 

GridView::widget([
    'dataProvider'=>$dataProvider,
    'summary'=>"Mostrando {begin} - {end} de {totalCount} elementos",
     'tableOptions' => [
         'class' => 'table table-striped table-bordered fondoAzul',
         'style'=>['width:500px']
         ],
    'columns'=>[
            'id',
            'titulo',
            'texto',
        [
                       'attribute'=>'foto',
                       'format'=>'raw',
                        'value' => function ($datos) {
                            $url = "@web/imgs/".$datos->foto;
                            return Html::img($url, ['alt'=>'myImage','width'=>'500px']);
                        }
                        ],
    ]
]);
    ?>
<?= 

GridView::widget([
    'dataProvider'=>$dataProvider1,
    'summary'=>"Mostrando {begin} - {end} de {totalCount} elementos",
     'tableOptions' => [
         'class' => 'table table-striped table-bordered fondoAzul',
         'style'=>['width:500px']
         ],
    'columns'=>[
        //['class' => 'yii\grid\SerialColumn'],
        
            'id',
            'titulo',
            'texto',
        [
                       'attribute'=>'foto',
                       'format'=>'raw',
                        'value' => function ($datos) {
                            $url = "@web/imgs/".$datos->foto;
                            return Html::img($url, ['alt'=>'myImage','width'=>'500px']);
                        }
                        ],
    ]
]);
    ?>

<?php 
/*
$v = $dataProvider->getPagination();
echo $v;
$v1 = $dataProvider1->getPagination();
echo $v1;
*/
?>