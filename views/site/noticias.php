
<div class="site-index">
    <div class="row">
        <?php
        foreach ($noticias as $registro){
        ?>
      <div class="col-xs-6 col-md-3">
          <?= yii\helpers\Html::img("@web/imgs/".$registro->foto,[
              'class'=>'img-responsive img-rounded',
              'style'=>'margin-top:10px',
          ])?>
          <?= "<h2>$registro->titulo</h2>" ?>
          <?= "<p>$registro->texto</p>" ?>
      </div>
        
       <?php 
        }
       ?>
        
    </div>
    
</div>