<?php
use yii\helpers\Html;

?>
<div class="site-index">
    <div class="row">
        <?php
        foreach ($articulos as $reg){
        ?>
      <div class="col-xs-6 col-md-3">
          <?= yii\helpers\Html::img("@web/imgs/".$reg->foto,[
              'class'=>'img-responsive img-rounded',
              'style'=>'margin-top:10px',
          ])?>
          <?= "<h2>$reg->titulo</h2>" ?>
          <?= "<p>$reg->texto</p>" ?>
          <?= Html::a('Leer más', ['/site/articuloampliado',
              "id"=>$reg->id,
          ], 
             ['class'=>'btn btn-primary',
              ]) ?>
   
          
      </div>
        
       <?php 
        }
       ?>
        
    </div>
    
</div>