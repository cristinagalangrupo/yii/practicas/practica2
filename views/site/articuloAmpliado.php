<?php
use yii\helpers\Html;

//echo "$id";
//var_dump($articulos);
//echo $articulos->titulo;
//echo $articulos->texto;
?>

<div class="row">
  <div class="col-md-12 col-lg-8">
    <div class="thumbnail">
      <?= Html::img('@web/imgs/'.$articulos->foto, ['alt' => 'My logo', 'width'=>'900px']) ?>
      <div class="caption">
        <h3><?= $articulos->titulo ?></h3>
        <p><?= $articulos->texto ?></p>
        
      </div>
    </div>
  </div>
</div>