DROP DATABASE IF EXISTS practica2;
CREATE DATABASE practica2;
USE practica2;

CREATE TABLE noticias(
  id int AUTO_INCREMENT,
  titulo varchar(255),
  texto varchar(255),
  foto varchar(255),
  PRIMARY KEY(id)
  );
CREATE TABLE articulos(
  id int AUTO_INCREMENT,
  titulo varchar(255),
  texto varchar(255),
  textoLargo varchar(255),
  foto varchar(255),
  PRIMARY KEY(id)
  );
INSERT INTO noticias (titulo,texto,foto) VALUES
  ('T�tulo de la noticia 1', 'Texto de la noticia 1','foto1.jpg'),
  ('T�tulo de la noticia 2', 'Texto de la noticia 2','foto2.jpg');
INSERT INTO articulos(titulo,texto,textoLargo,foto) VALUES 
  ('T�tulo del art�culo 1','Texto de la noticia 1','Este es el texto largo del art�culo 1','foto3.jpg'),
  ('T�tulo del art�culo 2','Texto de la noticia 2','Este es el texto largo del art�culo 2','foto4.jpg'),
  ('T�tulo del art�culo 3','Texto de la noticia 3','Este es el texto largo del art�culo 3','foto5.jpg');